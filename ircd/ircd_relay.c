/*
 * IRC - Internet Relay Chat, ircd/ircd_relay.c
 * Copyright (C) 1990 Jarkko Oikarinen and
 *                    University of Oulu, Computing Center
 *
 * See file AUTHORS in IRC package for additional names of
 * the programmers.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 1, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
/** @file
 * @brief Helper functions to relay various types of messages.
 * @version $Id: ircd_relay.c,v 1.17 2004/12/11 05:13:45 klmitch Exp $
 *
 * There are four basic types of messages, each with four subtypes.
 *
 * The basic types are: channel, directed, masked, and private.
 * Channel messages are (perhaps obviously) sent directly to a
 * channel.  Directed messages are sent to "NICK[%host]@server", but
 * only allowed if the server is a services server (to avoid
 * information leaks for normal clients).  Masked messages are sent to
 * either *@*host.mask or *.server.mask.  Private messages are sent to
 * NICK.
 *
 * The subtypes for each type are: client message, client notice,
 * server message, and server notice.  Client subtypes are sent by a
 * local user, and server subtypes are given to us by a server.
 * Notice subtypes correspond to the NOTICE command, and message
 * subtypes correspond to the PRIVMSG command.
 *
 * As a special note, directed messages do not have server subtypes,
 * since there is no difference in handling them based on origin.
 */
#include "config.h"

#include "ircd_relay.h"
#include "channel.h"
#include "client.h"
#include "hash.h"
#include "ircd.h"
#include "ircd_chattr.h"
#include "ircd_features.h"
#include "ircd_log.h"
#include "ircd_reply.h"
#include "ircd_string.h"
#include "match.h"
#include "msg.h"
#include "numeric.h"
#include "numnicks.h"
#include "s_debug.h"
#include "s_misc.h"
#include "s_user.h"
#include "send.h"

/* #include <assert.h> -- Now using assert in ircd_log.h */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>  //for the tolower
#include <stddef.h>	//for the ptrdiff_t

char* str_tolower(const char* message)
{
    char* text = (char *)malloc(strlen(message) + 1 * sizeof(char));
    if(text == NULL){
        fprintf(stderr, "Could not allocate memory for our strcpy.\n");
        return NULL;
    }

    strcpy(text,message);

    int i;
    int j; j = 0;
    int k; k = 0;
    for (i = 0; text[i]; i++)
        text[i] = tolower(text[i]);
    return text;
}

char *replace_str(const char *str, const char *old, const char *new)
{
	char *ret, *r;
	const char *p, *q;
	size_t oldlen = strlen(old);
	size_t count, retlen, newlen = strlen(new);

	if (oldlen != newlen) {
		for (count = 0, p = str; (q = strstr(p, old)) != NULL; p = q + oldlen)
			count++;
		/* this is undefined if p - str > PTRDIFF_MAX */
		retlen = p - str + strlen(p) + count * (newlen - oldlen);
	} else
		retlen = strlen(str);

	if ((ret = malloc(retlen + 1)) == NULL)
		return NULL;

	for (r = ret, p = str; (q = strstr(p, old)) != NULL; p = q + oldlen) {
		/* this is undefined if q - p > PTRDIFF_MAX */
		ptrdiff_t l = q - p;
		memcpy(r, p, l);
		r += l;
		memcpy(r, new, newlen);
		r += newlen;
	}
	strcpy(r, p);

	return ret;
}

int checkIfValidMessage(const char* message)
{
    if (strstr(message,"h-t-t-p") != 0) return 0;
    if (strstr(message,"w w w") != 0) return 0;
	if (strstr(message,"w.w.w") != 0) return 0;
    if (strstr(message,"w,w,w") != 0) return 0;
    if (strstr(message,"w-w-w") != 0) return 0;
	if (strstr(message,"id") != 0) return 0;
    if (strstr(message,".o r g") != 0) return 0;
    if (strstr(message,".c o m") != 0) return 0;
   if (strstr(message,"chat-online") != 0) return 0;
   if (strstr(message,"el-ea.info") != 0) return 0;
   if (strstr(message,"cheatromantic.ro") != 0) return 0;
   if (strstr(message,"desirenet.net") != 0) return 0;
   if (strstr(message,"bongacamsfree.eu") != 0) return 0;
   if (strstr(message,"rowebchat") != 0) return 0;
   if (strstr(message,"chatromania") != 0) return 0;
   if (strstr(message,"romaniachat") != 0) return 0;
   if (strstr(message,"chatromania") != 0) return 0;
    if (strstr(message,".e u") != 0) return 0;
    if (strstr(message,".n e t") != 0) return 0;
    if (strstr(message,".r o") != 0) return 0;
    if (strstr(message,".t k") != 0) return 0;
    if (strstr(message,".u s") != 0) return 0;
    if (strstr(message,"thunder") != 0) return 0;
    if (strstr(message,"/s ") != 0) return 0;
    if (strstr(message,"/server") != 0) return 0;
    if (strstr(message,"/s -m") != 0) return 0;
    if (strstr(message,"s e r v e r") != 0) return 0;
    if (strstr(message,"desirenet.org") != 0) return 1;
    if (strstr(message,"chatcuweb.com") != 0) return 1;
    if (strstr(message,"chat-online.info") != 0) return 1;
    if (strstr(message,"chatulfetelor.net") != 0) return 1;
    if (strstr(message,"facebook.com") != 0) return 1;
    if (strstr(message,"youtube.com") != 0) return 1;
    if (strstr(message,"yahoo.com") != 0) return 1;
    if (strstr(message,"gmail.com") != 0) return 1;
    if (strstr(message,"http://") != 0) return 0;
    if (strstr(message,"h t t p ://") != 0) return 0;
    if (strstr(message,"www.") != 0) return 0;
    if (strstr(message,"hello") != 0)		//trying to let gnuworld's HELLO command to pass through
    {
    	if (strstr(message,"@") != 0) //&& (strstr(message,".") != 0)
    		return 1;
    }
    if (strstr(message,".ro") != 0) return 0;
    if (strstr(message,".com") != 0) return 0;
	if (strstr(message,"punct") != 0) return 0;
    if (strstr(message,".org") != 0) return 0;
    if (strstr(message,".net") != 0) return 0;
    if (strstr(message,".tk") != 0) return 0;
    if (strstr(message,".info") != 0) return 0;
    if (strstr(message,".free") != 0) return 0;
    if (strstr(message,".biz") != 0) return 0;
    if (strstr(message,".be") != 0) return 0;
    if (strstr(message,".edu") != 0) return 0;
    if (strstr(message,".gov") != 0) return 0;
    if (strstr(message,".us ") != 0) return 0;
    if (strstr(message,".eu ") != 0) return 0;
    if (strstr(message,".de ") != 0) return 0;
    if (strstr(message,".ca ") != 0) return 0;
    if (strstr(message,"cupleaza-te.com") != 0) return 0;
    if (strstr(message,".senzatie.net") != 0) return 0;
    if (strstr(message,"starchatromania.org") != 0) return 0;
    if (strstr(message,"s t a r c h a t r o m a n i a .org") != 0) return 0;
    if (strstr(message,".fr ") != 0) return 0;
    if (strstr(message," irc.") != 0) return 0;
    if (strstr(message,"senzatie.") != 0) return 0;
    if (strstr(message,"chatmusic") != 0) return 0;
    if (strstr(message,"radioclick") != 0) return 0;
    if (strstr(message,"goo.gl/") != 0) return 0;
    if (strstr(message,"0768000222") != 0) return 0;
    if (strstr(message,"0768 000 222") != 0) return 0;
    if (strstr(message,"0768-000-222") != 0) return 0;
    return 1;
}

char* validateWords(const char* message)
{
	const char* text = (const char *)str_tolower(message);
	char *pch = (char *)text;
	int retpch; retpch = 0;
	if (strstr(text, "pula") != 0) { pch = replace_str(pch, "pula", "floare"); retpch = 1; }
	if (strstr(text, " pu la ") != 0) { pch = replace_str(pch, " pu la ", " floare "); retpch = 1; }
	if (strstr(text, "pu-la") != 0) { pch = replace_str(pch, "pu-la", "floare"); retpch = 1; }
	if (strstr(text, "pizda") != 0) { pch = replace_str(pch, "pizda", "painea"); retpch = 1; }
	if (strstr(text, "muie") != 0) { pch = replace_str(pch, "muie", "pupic"); retpch = 1; }
	if (strstr(text, "mata ") != 0) { pch = replace_str(pch, "mata ", "iubita "); retpch = 1; }
	if (strstr(text, "ma-ta ") != 0) { pch = replace_str(pch, "ma-ta ", "iubita "); retpch = 1; }
	if (strstr(text, "matii ") != 0) { pch = replace_str(pch, "matii ", "iubita "); retpch = 1; }
	if (strstr(text, " la masi") != 0) { pch = replace_str(pch, " la masi", " lui iubita"); retpch = 1; }
	if (strstr(text, "sugi") != 0) { pch = replace_str(pch, "sugi", "iubesti"); retpch = 1; }
	if (strstr(text, "suge") != 0) { pch = replace_str(pch, "suge", "iubeste"); retpch = 1; }
	if (strstr(text, "supt") != 0) { pch = replace_str(pch, "supt", "iubit"); retpch = 1; }
	if (strstr(text, "prost") != 0) { pch = replace_str(pch, "prost", "destept"); retpch = 1; }
	if (strstr(text, "proast") != 0) { pch = replace_str(pch, "proast", "desteapta"); retpch = 1; }
	if (strstr(text, "idiot") != 0) { pch = replace_str(pch, "idiot", "inteligent"); retpch = 1; }
	if (strstr(text, "tarfa") != 0) { pch = replace_str(pch, "tarfa", "frumoasa"); retpch = 1; }
	if (strstr(text, "sluga") != 0) { pch = replace_str(pch, "sluga", "printesa"); retpch = 1; }
	if (strstr(text, "chat") != 0) { pch = replace_str(pch, "chat", "chat desirenet"); retpch = 1; }
	if (strstr(text, "handicapat") != 0) { pch = replace_str(pch, "handicapat", "frumos"); retpch = 1; }
	if (strstr(text, "handikap") != 0) { pch = replace_str(pch, "handikap", "super"); retpch = 1; }
	if (strstr(text, "cacat") != 0) { pch = replace_str(pch, "cacat", "bomboane"); retpch = 1; }
	if (strstr(text, "poponar") != 0) { pch = replace_str(pch, "poponar", "casatorit"); retpch = 1; }
	if (strstr(text, "fatalau") != 0) { pch = replace_str(pch, "fatalau", "gigolo"); retpch = 1; }
	if (strstr(text, "ungur") != 0) { pch = replace_str(pch, "ungur", "roman"); retpch = 1; }
	if (strstr(text, "diabet") != 0) { pch = replace_str(pch, "diabet", "dulce"); retpch = 1; }
	if (strstr(text, " fut") != 0) { pch = replace_str(pch, " fut", " iubi"); retpch = 1; }
	if (strstr(text, "futu") != 0) { pch = replace_str(pch, "futu", "iubesc"); retpch = 1; }
	if (strstr(text, " mort") != 0) { pch = replace_str(pch, " mort", "pisic"); retpch = 1; }
	if (strstr(text, " moara") != 0) { pch = replace_str(pch, " moara", " creasca"); retpch = 1; }
	if (strstr(text, "dobitoc") != 0) { pch = replace_str(pch, "dobitoc", "cuminte"); retpch = 1; }
	if (strstr(text, "muist") != 0) { pch = replace_str(pch, "muist", "frumos"); retpch = 1; }
	if (strstr(text, "umile") != 0) { pch = replace_str(pch, "umile", "dive"); retpch = 1; }
	if (strstr(text, " bou") != 0) { pch = replace_str(pch, " bou", " erou"); retpch = 1; }
	if (strstr(text, "plm") != 0) { pch = replace_str(pch, "plm", "dulce"); retpch = 1; }
	if (strstr(text, "debil") != 0) { pch = replace_str(pch, "debil", "abil"); retpch = 1; }
        if (strstr(text, "DANY") != 0) { pch = replace_str(pch, "DANY", "DANY SEFU"); retpch = 1; }
         if (strstr(text, "dany") != 0) { pch = replace_str(pch, "dany", "DANY SEFU"); retpch = 1; }        
if (strstr(text, "Zamolxes") != 0) { pch = replace_str(pch, "Zamolxes", "Zamolxes SEFU"); retpch = 1; }
        if (strstr(text, "profitor") != 0) { pch = replace_str(pch, "profitor", "dulce"); retpch = 1; }
        if (strstr(text, "rh") != 0) { pch = replace_str(pch, "rh", "rh SEFU"); retpch = 1; }
if (strstr(text, "victor") != 0) { pch = replace_str(pch, "victor", "VICTOR SEFU"); retpch = 1; }
if (strstr(text, "zamolxes") != 0) { pch = replace_str(pch, "zamolxes", "Zamolxes SEFU"); retpch = 1; }
if (strstr(text, "badboys") != 0) { pch = replace_str(pch, "badboys", "Romania"); retpch = 1; }
        if (strstr(text, "Insane4Ever") != 0) { pch = replace_str(pch, "Insane4Ever", "Insane4Ever SEFU"); retpch = 1; }
        if (strstr(text, "cosmin") != 0) { pch = replace_str(pch, "cosmin", "cosmine rh"); retpch = 1; }
        if (strstr(text, "insane4ever") != 0) { pch = replace_str(pch, "insane4ever", "Insane4Ever SEFU"); retpch = 1; }
if (strstr(text, "milog") != 0) { pch = replace_str(pch, "milog", "boss SEFU"); retpch = 1; }
if (strstr(text, "cersetor") != 0) { pch = replace_str(pch, "cersetor", "boss SEFU"); retpch = 1; }
        if (strstr(text, "hk") != 0) { pch = replace_str(pch, "hk", "hk SEFU"); retpch = 1; }
        if (strstr(text, "bani") != 0) { pch = replace_str(pch, "bani", "bomboane"); retpch = 1; }
        if (strstr(text, "malanciuc") != 0) { pch = replace_str(pch, "malanciuc", "rh"); retpch = 1; }
	if (strstr(text, "bucuresti") != 0) { pch = replace_str(pch, "bucuresti", "Bucuresti SEFU"); retpch = 1; }
	if (strstr(text, "fotomodelu") != 0) { pch = replace_str(pch, "fotomodelu", "Fotomodelu SEFU"); retpch = 1; }
	if (strstr(text, "lachet") != 0) { pch = replace_str(pch, "lachet", "patron"); retpch = 1; }
	if (strstr(text, " pis ") != 0) { pch = replace_str(pch, " pis ", " pup "); retpch = 1; }
	if (strstr(text, " pla ") != 0) { pch = replace_str(pch, " pla ", "bani"); retpch = 1; }
	if (strstr(text, "retard") != 0) { pch = replace_str(pch, "retard", "bogat"); retpch = 1; }
	if (strstr(text, "pofloarera") != 0) { pch = replace_str(pch, "pofloarera", "populara"); retpch = 1; }
	if (strstr(text, "javra") != 0) { pch = replace_str(pch, "javra", "amabil"); retpch = 1; }
	if (strstr(text, "gunoi") != 0) { pch = replace_str(pch, "gunoi", "scump"); retpch = 1; }
	if (strstr(text, "curva") != 0) { pch = replace_str(pch, "curva", "superba"); retpch = 1; }
	if (strstr(text, "sictir") != 0) { pch = replace_str(pch, "sictir", "pupici"); retpch = 1; }
	if (strstr(text, "undernet") != 0) { pch = replace_str(pch, "undernet", "DesireNET"); retpch = 1; }
    if (strstr(text, "mirc") != 0) { pch = replace_str(pch, "mirc", "DesireNET"); retpch = 1; }
    if (strstr(text, "romantic") != 0) { pch = replace_str(pch, "romantic", "DesireNET"); retpch = 1; }
    if (strstr(text, "chatmusic") != 0) { pch = replace_str(pch, "chatmusic", "DesireNET"); retpch = 1; }
    if (strstr(text, "desirenet.net") != 0) { pch = replace_str(pch, "desirenet.net", "DesireNET.Org"); retpch = 1; }
    if (strstr(text, "http") != 0) { pch = replace_str(pch, "http", "DesireNET"); retpch = 1; }
    if (strstr(text, "h t t p") != 0) { pch = replace_str(pch, "h t t p", "DesireNET"); retpch = 1; }
    if (strstr(text, "www") != 0) { pch = replace_str(pch, "www", "DesireNET"); retpch = 1; }
    if (strstr(text, "w w w") != 0) { pch = replace_str(pch, "w w w", "DesireNET"); retpch = 1; }
    if (strstr(text, "radioclick") != 0) { pch = replace_str(pch, "radioclick", "DesireNET"); retpch = 1; }
    if (strstr(text, "rosexchat") != 0) { pch = replace_str(pch, "rosexchat", "DesireNET"); retpch = 1; }
    if (strstr(text, "bonga") != 0) { pch = replace_str(pch, "bonga", "DesireNET"); retpch = 1; }
    if (strstr(text, "chatromantic") != 0) { pch = replace_str(pch, "chatromantic", "DesireNET"); retpch = 1; }
    if (strstr(text, "spam") != 0) { pch = replace_str(pch, "spam", "nu se poate face spam pe DesireNET"); retpch = 1; }
    if (strstr(text, "chat-online") != 0) { pch = replace_str(pch, "chat-online", "DesireNET"); retpch = 1; }
    if (strstr(text, "el-ea") != 0) { pch = replace_str(pch, "el-ea", "DesireNET"); retpch = 1; }
    if (strstr(text, "cheatromania") != 0) { pch = replace_str(pch, "cheatromania", "DesireNET"); retpch = 1; }
    if (strstr(text, "socializam") != 0) { pch = replace_str(pch, "socializam", "DesireNET"); retpch = 1; }
    if (strstr(text, "criminel") != 0) { pch = replace_str(pch, "criminel", "DesireNET"); retpch = 1; }
    if (strstr(text, "foartetare") != 0) { pch = replace_str(pch, "foartetare", "DesireNET"); retpch = 1; }
    if (strstr(text, "mess") != 0) { pch = replace_str(pch, "mess", "DesireNET"); retpch = 1; }
    if (strstr(text, "fb") != 0) { pch = replace_str(pch, "fb", "DesireNET"); retpch = 1; }
    if (strstr(text, "facebook") != 0) { pch = replace_str(pch, "facebook", "DesireNET"); retpch = 1; }
        if (strstr(text, "chatromantic") != 0) { pch = replace_str(pch, "chatromantic", "DesireNET"); retpch = 1; }
		//RoyNelson section
	if (strstr(text, "foot ") != 0) { pch = replace_str(pch, "foot ", "daruiesc"); retpch = 1; }
	if (strstr(text, "swgi") != 0) { pch = replace_str(pch, "swgi", "dai"); retpch = 1; }
	if (strstr(text, "shwg") != 0) { pch = replace_str(pch, "shwg", "iubesc"); retpch = 1; }
	if (strstr(text, "pwla") != 0) { pch = replace_str(pch, "pwla", "floarea"); retpch = 1; }
	if (strstr(text, "phwla") != 0) { pch = replace_str(pch, "phwla", "floarea"); retpch = 1; }
	if (strstr(text, "fhwt") != 0) { pch = replace_str(pch, "fhwt", "floare"); retpch = 1; }
	if (strstr(text, "sa-mi bag") != 0) { pch = replace_str(pch, "sa-mi bag", "sa daruiesc"); retpch = 1; }
	if (retpch)
		return pch;
	else
		return (char *)message;
}

int isValidMessage(const char* message)
{
    char* text = str_tolower(message);

    int is_valid;
//    char *text2 = validateWords((const char *)text);
    is_valid = checkIfValidMessage((const char *)text);
    free(text);
    return is_valid;

    /*int thelen = strlen(text) / 2 + 1;
    char* text2 = (char *)malloc(thelen * sizeof(char));
    if(text == NULL){
        fprintf(stderr, "Could not allocate memory for our strcpy.\n");
        return -1;
    }
    for (i = 0; text[i];)
    {
        text2[i] += text[j];
        //if (text[i] == '-')	// <- Enable this for bad character counting !!
        //	k++;
        i++;
        j += 2;
    }

    if (!checkIfValidMessage(text2))
    	return 0;

    //If bad charcter count is greater then a number, reject ...
    // Careful with this!
    //if (k > 3)
    //	return 0;
	*/
    //return 1;
}
/*
 * This file contains message relaying functions for client and server
 * private messages and notices
 * TODO: This file contains a lot of cut and paste code, and needs
 * to be cleaned up a bit. The idea is to factor out the common checks
 * but not introduce any IsOper/IsUser/MyUser/IsServer etc. stuff.
 */

/** Relay a local user's message to a channel.
 * Generates an error if the client cannot send to the channel.
 * @param[in] sptr Client that originated the message.
 * @param[in] name Name of target channel.
 * @param[in] text %Message to relay.
 * @param[in] targetc Count of channels we're sending the message to.
 */
void relay_channel_message(struct Client* sptr, const char* name, const char* text, const int targetc)
{
  struct Channel* chptr;
  const char *ch;

  struct Membership *member;

  assert(0 != sptr);
  assert(0 != name);
  assert(0 != text);

  if (0 == (chptr = FindChannel(name))) {
    send_reply(sptr, ERR_NOSUCHCHANNEL, name);
    return;
  }
  /*
   * This first: Almost never a server/service
   */
  if (!client_can_send_to_channel(sptr, chptr, 1)) {
    send_reply(sptr, ERR_CANNOTSENDTOCHAN, chptr->chname);
    return;
  }
  if ((chptr->mode.mode & MODE_NOPRIVMSGS) &&
      check_target_limit(sptr, chptr, chptr->chname, 0))
    return;
  
  ClrFlag(sptr, FLAG_TS8);

  member = find_member_link(chptr, sptr);

  if ((member) && (!IsChanOp(member)))
  if ((!isValidMessage(text)) && (!IsOper(sptr)) && (!IsService(sptr)) && (!IsServer(sptr)))
  {
          Debug((DEBUG_ERROR, "FILTERED <<%s> -> <%s>> %s", cli_name(sptr), chptr->chname, text));
          return;
  }
  else
          Debug((DEBUG_ERROR, "<<%s> -> <%s>> %s", cli_name(sptr), chptr->chname, text));

  const char* text2;

  if (member)
  if ((!IsOper(sptr)) && (!IsService(sptr)) && (!IsServer(sptr)))
	  text2 = (const char *)validateWords(text);
  else
	  text2 = text;

  sendcmdto_channel_butone(sptr, CMD_PRIVATE, chptr, cli_from(sptr),
			   SKIP_DEAF | SKIP_BURST, "%H :%s", chptr, text2);
} 

/** Relay a local user's notice to a channel.
 * Silently exits if the client cannot send to the channel.
 * @param[in] sptr Client that originated the message.
 * @param[in] name Name of target channel.
 * @param[in] text %Message to relay.
 * @param[in] targetc Count of channels we're sending the notice to.
 */
void relay_channel_notice(struct Client* sptr, const char* name, const char* text, const int targetc)
{
  struct Channel* chptr;
  const char *ch;
  assert(0 != sptr);
  assert(0 != name);
  assert(0 != text);

  struct Membership *member;

  if (0 == (chptr = FindChannel(name)))
    return;
  /*
   * This first: Almost never a server/service
   */
  if (!client_can_send_to_channel(sptr, chptr, 1))
    return;

  if ((chptr->mode.mode & MODE_NOPRIVMSGS) &&
      check_target_limit(sptr, chptr, chptr->chname, 0))
    return;

  ClrFlag(sptr, FLAG_TS8);

  member = find_member_link(chptr, sptr);

  if ((member) && (!IsChanOp(member)))
  if ((!isValidMessage(text)) && (!IsOper(sptr)) && (!IsService(sptr)) && (!IsServer(sptr))) return;

  const char* text2;

  if (member)
  if ((!IsOper(sptr)) && (!IsService(sptr)) && (!IsServer(sptr)))
	  text2 = (const char *)validateWords(text);
  else
	  text2 = text;

  sendcmdto_channel_butone(sptr, CMD_NOTICE, chptr, cli_from(sptr),
			   SKIP_DEAF | SKIP_BURST, "%H :%s", chptr, text2);
}

/** Relay a message to a channel.
 * Generates an error if the client cannot send to the channel,
 * or if the channel is a local channel
 * @param[in] sptr Client that originated the message.
 * @param[in] name Name of target channel.
 * @param[in] text %Message to relay.
 */
void server_relay_channel_message(struct Client* sptr, const char* name, const char* text)
{
  struct Channel* chptr;
  assert(0 != sptr);
  assert(0 != name);
  assert(0 != text);

  if (IsLocalChannel(name) || 0 == (chptr = FindChannel(name))) {
    send_reply(sptr, ERR_NOSUCHCHANNEL, name);
    return;
  }
  /*
   * This first: Almost never a server/service
   * Servers may have channel services, need to check for it here
   */
  if (client_can_send_to_channel(sptr, chptr, 1) || IsChannelService(sptr)) {
    sendcmdto_channel_butone(sptr, CMD_PRIVATE, chptr, cli_from(sptr),
			     SKIP_DEAF | SKIP_BURST, "%H :%s", chptr, text);
  }
  else
    send_reply(sptr, ERR_CANNOTSENDTOCHAN, chptr->chname);
}

/** Relay a notice to a channel.
 * Generates an error if the client cannot send to the channel,
 * or if the channel is a local channel
 * @param[in] sptr Client that originated the message.
 * @param[in] name Name of target channel.
 * @param[in] text %Message to relay.
 */
void server_relay_channel_notice(struct Client* sptr, const char* name, const char* text)
{
  struct Channel* chptr;
  assert(0 != sptr);
  assert(0 != name);
  assert(0 != text);

  if (IsLocalChannel(name) || 0 == (chptr = FindChannel(name)))
    return;
  /*
   * This first: Almost never a server/service
   * Servers may have channel services, need to check for it here
   */
  if (client_can_send_to_channel(sptr, chptr, 1) || IsChannelService(sptr)) {
    sendcmdto_channel_butone(sptr, CMD_NOTICE, chptr, cli_from(sptr),
			     SKIP_DEAF | SKIP_BURST, "%H :%s", chptr, text);
  }
}

/** Relay a directed message.
 * Generates an error if the named server does not exist, if it is not
 * a services server, or if \a name names a local user and a hostmask
 * is specified but does not match.
 * @param[in] sptr Client that originated the message.
 * @param[in] name Target nickname, with optional "%hostname" suffix.
 * @param[in] server Name of target server.
 * @param[in] text %Message to relay.
 */
void relay_directed_message(struct Client* sptr, char* name, char* server, const char* text)
{
  struct Client* acptr;
  char*          host;

  assert(0 != sptr);
  assert(0 != name);
  assert(0 != text);
  assert(0 != server);

  if ((acptr = FindServer(server + 1)) == NULL || !IsService(acptr))
  {
    send_reply(sptr, ERR_NOSUCHNICK, name);
    return;
  }
  /*
   * NICK[%host]@server addressed? See if <server> is me first
   */
  if (!IsMe(acptr))
  {
    sendcmdto_one(sptr, CMD_PRIVATE, acptr, "%s :%s", name, text);
    return;
  }
  /*
   * Look for an user whose NICK is equal to <name> and then
   * check if it's hostname matches <host> and if it's a local
   * user.
   */
  *server = '\0';
  if ((host = strchr(name, '%')))
    *host++ = '\0';

  /* As reported by Vampire-, it's possible to brute force finding users
   * by sending a message to each server and see which one succeeded.
   * This means we have to remove error reporting.  Sigh.  Better than
   * removing the ability to send directed messages to client servers 
   * Thanks for the suggestion Vampire=.  -- Isomer 2001-08-28
   * Argh, /ping nick@server, disallow messages to non +k clients :/  I hate
   * this. -- Isomer 2001-09-16
   */
  if (!(acptr = FindUser(name)) || !MyUser(acptr) ||
      (!EmptyString(host) && 0 != match(host, cli_user(acptr)->host)) ||
      !IsChannelService(acptr))
  {
    /*
     * By this stage we might as well not bother because they will
     * know that this server is currently linked because of the
     * increased lag.
     */
    send_reply(sptr, ERR_NOSUCHNICK, name);
    return;
  }

  *server = '@';
  if (host)
    *--host = '%';

  if (!(is_silenced(sptr, acptr)))
  {
	  Debug((DEBUG_ERROR, "<<%s> -> <%s>> %s", cli_name(sptr), cli_name(acptr), text));	//DIRECTED!
    sendcmdto_one(sptr, CMD_PRIVATE, acptr, "%s :%s", name, text);
  }
}

/** Relay a directed notice.
 * Generates an error if the named server does not exist, if it is not
 * a services server, or if \a name names a local user and a hostmask
 * is specified but does not match.
 * @param[in] sptr Client that originated the message.
 * @param[in] name Target nickname, with optional "%hostname" suffix.
 * @param[in] server Name of target server.
 * @param[in] text %Message to relay.
 */
void relay_directed_notice(struct Client* sptr, char* name, char* server, const char* text)
{
  struct Client* acptr;
  char*          host;

  assert(0 != sptr);
  assert(0 != name);
  assert(0 != text);
  assert(0 != server);

  if (0 == (acptr = FindServer(server + 1)))
    return;
  /*
   * NICK[%host]@server addressed? See if <server> is me first
   */
  if (!IsMe(acptr)) {
    sendcmdto_one(sptr, CMD_NOTICE, acptr, "%s :%s", name, text);
    return;
  }
  /*
   * Look for an user whose NICK is equal to <name> and then
   * check if it's hostname matches <host> and if it's a local
   * user.
   */
  *server = '\0';
  if ((host = strchr(name, '%')))
    *host++ = '\0';

  if (!(acptr = FindUser(name)) || !MyUser(acptr) ||
      (!EmptyString(host) && 0 != match(host, cli_user(acptr)->host)))
    return;

  *server = '@';
  if (host)
    *--host = '%';

  if (!(is_silenced(sptr, acptr)))
    sendcmdto_one(sptr, CMD_NOTICE, acptr, "%s :%s", name, text);
}

/** Relay a private message from a local user.
 * Returns an error if the user does not exist or sending to him would
 * exceed the source's free targets.  Sends an AWAY status message if
 * the target is marked as away.
 * @param[in] sptr Client that originated the message.
 * @param[in] name Nickname of target user.
 * @param[in] text %Message to relay.
 */
void relay_private_message(struct Client* sptr, const char* name, const char* text)
{
  struct Client* acptr;

  assert(0 != sptr);
  assert(0 != name);
  assert(0 != text);

  if (0 == (acptr = FindUser(name))) {
    send_reply(sptr, ERR_NOSUCHNICK, name);
    return;
  }
  if ((!IsChannelService(acptr) &&
       check_target_limit(sptr, acptr, cli_name(acptr), 0)) ||
      is_silenced(sptr, acptr))
    return;


  /*
   * send away message if user away
   */
  if (cli_user(acptr) && cli_user(acptr)->away)
    send_reply(sptr, RPL_AWAY, cli_name(acptr), cli_user(acptr)->away);

  //  if ((!isValidMessage(text)) && (!IsOper(sptr)) && (!IsService(sptr)) && (!IsServer(sptr))) return;
  if ((!isValidMessage(text)) && (!IsOper(sptr)) && (!IsService(sptr)) && (!IsServer(sptr)) && (!IsOper(acptr)))
  {
          Debug((DEBUG_ERROR, "FILTERED <<%s> -> <%s>> %s", cli_name(sptr), cli_name(acptr), text));
          return;
  }
  else
          Debug((DEBUG_ERROR, "<<%s> -> <%s>> %s", cli_name(sptr), cli_name(acptr), text));

  /*
   * deliver the message
   */
  if (MyUser(acptr))
    add_target(acptr, sptr);

  const char* text2;

  if ((!IsOper(sptr)) && (!IsService(sptr)) && (!IsServer(sptr)))
	  text2 = (const char *)validateWords(text);
  else
	  text2 = text;

  sendcmdto_one(sptr, CMD_PRIVATE, acptr, "%C :%s", acptr, text2);
}

/** Relay a private notice from a local user.
 * Returns an error if the user does not exist or sending to him would
 * exceed the source's free targets.  Sends an AWAY status message if
 * the target is marked as away.
 * @param[in] sptr Client that originated the message.
 * @param[in] name Nickname of target user.
 * @param[in] text %Message to relay.
 */
void relay_private_notice(struct Client* sptr, const char* name, const char* text)
{
  struct Client* acptr;
  assert(0 != sptr);
  assert(0 != name);
  assert(0 != text);

  if (0 == (acptr = FindUser(name)))
    return;
  if ((!IsChannelService(acptr) && 
       check_target_limit(sptr, acptr, cli_name(acptr), 0)) ||
      is_silenced(sptr, acptr))
    return;

//if ((!isValidMessage(text)) && (!IsOper(sptr)) && (!IsService(sptr)) && (!IsServer(sptr)) && (!IsOper(acptr))) return;
  if ((!isValidMessage(text)) && (!IsOper(sptr)) && (!IsService(sptr)) && (!IsServer(sptr)) && (!IsOper(acptr)))
  {
          Debug((DEBUG_ERROR, "FILTERED NOTICE <<%s> -> <%s>> %s", cli_name(sptr), cli_name(acptr), text));
          return;
  }
  else
          Debug((DEBUG_ERROR, "<<%s> N-> <%s>> %s", cli_name(sptr), cli_name(acptr), text));

  if (MyUser(acptr))
    add_target(acptr, sptr);

  const char* text2;

  if ((!IsOper(sptr)) && (!IsService(sptr)) && (!IsServer(sptr)))
	  text2 = (const char *)validateWords(text);
  else
	  text2 = text;

  sendcmdto_one(sptr, CMD_NOTICE, acptr, "%C :%s", acptr, text2);
}

/** Relay a private message that arrived from a server.
 * Returns an error if the user does not exist.
 * @param[in] sptr Client that originated the message.
 * @param[in] name Nickname of target user.
 * @param[in] text %Message to relay.
 */
void server_relay_private_message(struct Client* sptr, const char* name, const char* text)
{
  struct Client* acptr;
  assert(0 != sptr);
  assert(0 != name);
  assert(0 != text);
  /*
   * nickname addressed?
   */
  if (0 == (acptr = findNUser(name)) || !IsUser(acptr)) {
    send_reply(sptr, SND_EXPLICIT | ERR_NOSUCHNICK, "* :Target left %s. "
	       "Failed to deliver: [%.20s]", feature_str(FEAT_NETWORK),
               text);
    return;
  }
  if (is_silenced(sptr, acptr))
    return;

  if (MyUser(acptr))
    add_target(acptr, sptr);

  Debug((DEBUG_ERROR, "<<%s> -> <%s>> %s", cli_name(sptr), cli_name(acptr), text));
  sendcmdto_one(sptr, CMD_PRIVATE, acptr, "%C :%s", acptr, text);
}


/** Relay a private notice that arrived from a server.
 * Returns an error if the user does not exist.
 * @param[in] sptr Client that originated the message.
 * @param[in] name Nickname of target user.
 * @param[in] text %Message to relay.
 */
void server_relay_private_notice(struct Client* sptr, const char* name, const char* text)
{
  struct Client* acptr;
  assert(0 != sptr);
  assert(0 != name);
  assert(0 != text);
  /*
   * nickname addressed?
   */
  if (0 == (acptr = findNUser(name)) || !IsUser(acptr))
    return;

  if (is_silenced(sptr, acptr))
    return;

  if (MyUser(acptr))
    add_target(acptr, sptr);

  Debug((DEBUG_ERROR, "<<%s> N-> <%s>> %s", cli_name(sptr), cli_name(acptr), text));
  sendcmdto_one(sptr, CMD_NOTICE, acptr, "%C :%s", acptr, text);
}

/** Relay a masked message from a local user.
 * Sends an error response if there is no top-level domain label in \a
 * mask, or if that TLD contains a wildcard.
 * @param[in] sptr Client that originated the message.
 * @param[in] mask Target mask for the message.
 * @param[in] text %Message to relay.
 */
void relay_masked_message(struct Client* sptr, const char* mask, const char* text)
{
  const char* s;
  int   host_mask = 0;

  assert(0 != sptr);
  assert(0 != mask);
  assert(0 != text);
  /*
   * look for the last '.' in mask and scan forward
   */
  if (0 == (s = strrchr(mask, '.'))) {
    send_reply(sptr, ERR_NOTOPLEVEL, mask);
    return;
  }
  while (*++s) {
    if (*s == '.' || *s == '*' || *s == '?')
       break;
  }
  if (*s == '*' || *s == '?') {
    send_reply(sptr, ERR_WILDTOPLEVEL, mask);
    return;
  }
  s = mask;
  if ('@' == *++s) {
    host_mask = 1;
    ++s;
  }

  Debug((DEBUG_ERROR, "(MASKED) <<%s> -> <%s>> %s", cli_name(sptr), mask, text));

  sendcmdto_match_butone(sptr, CMD_PRIVATE, s,
			 IsServer(cli_from(sptr)) ? cli_from(sptr) : 0,
			 host_mask ? MATCH_HOST : MATCH_SERVER,
			 "%s :%s", mask, text);
}

/** Relay a masked notice from a local user.
 * Sends an error response if there is no top-level domain label in \a
 * mask, or if that TLD contains a wildcard.
 * @param[in] sptr Client that originated the message.
 * @param[in] mask Target mask for the message.
 * @param[in] text %Message to relay.
 */
void relay_masked_notice(struct Client* sptr, const char* mask, const char* text)
{
  const char* s;
  int   host_mask = 0;

  assert(0 != sptr);
  assert(0 != mask);
  assert(0 != text);
  /*
   * look for the last '.' in mask and scan forward
   */
  if (0 == (s = strrchr(mask, '.'))) {
    send_reply(sptr, ERR_NOTOPLEVEL, mask);
    return;
  }
  while (*++s) {
    if (*s == '.' || *s == '*' || *s == '?')
       break;
  }
  if (*s == '*' || *s == '?') {
    send_reply(sptr, ERR_WILDTOPLEVEL, mask);
    return;
  }
  s = mask;
  if ('@' == *++s) {
    host_mask = 1;
    ++s;
  }

  Debug((DEBUG_ERROR, "(MASKED NOTICE) <<%s> -> <%s>> %s", cli_name(sptr), mask, text));

  sendcmdto_match_butone(sptr, CMD_NOTICE, s,
			 IsServer(cli_from(sptr)) ? cli_from(sptr) : 0,
			 host_mask ? MATCH_HOST : MATCH_SERVER,
			 "%s :%s", mask, text);
}

/** Relay a masked message that arrived from a server.
 * @param[in] sptr Client that originated the message.
 * @param[in] mask Target mask for the message.
 * @param[in] text %Message to relay.
 */
void server_relay_masked_message(struct Client* sptr, const char* mask, const char* text)
{
  const char* s = mask;
  int         host_mask = 0;
  assert(0 != sptr);
  assert(0 != mask);
  assert(0 != text);

  if ('@' == *++s) {
    host_mask = 1;
    ++s;
  }
  sendcmdto_match_butone(sptr, CMD_PRIVATE, s,
			 IsServer(cli_from(sptr)) ? cli_from(sptr) : 0,
			 host_mask ? MATCH_HOST : MATCH_SERVER,
			 "%s :%s", mask, text);
}

/** Relay a masked notice that arrived from a server.
 * @param[in] sptr Client that originated the message.
 * @param[in] mask Target mask for the message.
 * @param[in] text %Message to relay.
 */
void server_relay_masked_notice(struct Client* sptr, const char* mask, const char* text)
{
  const char* s = mask;
  int         host_mask = 0;
  assert(0 != sptr);
  assert(0 != mask);
  assert(0 != text);

  if ('@' == *++s) {
    host_mask = 1;
    ++s;
  }
  sendcmdto_match_butone(sptr, CMD_NOTICE, s,
			 IsServer(cli_from(sptr)) ? cli_from(sptr) : 0,
			 host_mask ? MATCH_HOST : MATCH_SERVER,
			 "%s :%s", mask, text);
}

